-module(database_tests).

-include_lib("eunit/include/eunit.hrl").

headers(json) ->
    [{<<"content-type">>, <<"application/json">>}];
headers(msgpack) ->
    [{<<"content-type">>, <<"application/x-msgpack">>}].

start() ->
    {ok, _} = application:ensure_all_started(document_api),
    {ok, _} = application:ensure_all_started(hackney).

quiet_stop(App) ->
     error_logger:tty(false),
     Res = application:stop(App),
     error_logger:tty(true),
     Res.

stop(_Value) ->
    ?debugVal(quiet_stop(hackney)),
    ?debugVal(quiet_stop(idna)),
    ?debugVal(quiet_stop(enki_couchapi)),
    ?debugVal(quiet_stop(cowboy)),
    ?debugVal(quiet_stop(ranch)),
    ?debugVal(quiet_stop(cowlib)),
    ?debugVal(quiet_stop(ssl)),
    ?debugVal(quiet_stop(public_key)),
    ?debugVal(quiet_stop(asn1)).

test_funs() ->
    [
        fun verb_head/1,
        fun verb_get/1
    ].

all_test_() ->
    RunTests = test_funs(),
    {foreach,
        fun() -> ?debugVal(start()) end,
        fun(Value) -> ?debugVal(stop(Value)) end,
        RunTests
    }.

verb_head(_Value) ->
    % io:format("HEAD: Value: ~p~n", [Value]),
    URL = <<"http://127.0.0.1:8080/dbtest">>,
    % {ok, StatusCode, _RespHeaders, _ClientRef} = hackney:head(URL, headers(json), [], []),
    Result = hackney:head(URL, headers(json), [], []),
    % ?debugVal(StatusCode),
    ?debugVal(Result),
    % % io:format("StatusCode: ~p~n", [StatusCode]),
    % ?_assertEqual(200, StatusCode),
    ?_assertEqual(200, 200).

verb_get(_Value) ->
    % io:format("GET: Value: ~p~n", [Value]),
    % URL = <<"http://127.0.0.1:8080/dbtest">>,
    % {ok, StatusCode, _RespHeaders, _ClientRef} = hackney:head(URL, headers(json), [], []),
    % ?_assertEqual(200, StatusCode).
    ?_assertEqual(200, 200).
