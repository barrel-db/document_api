Compilation
-----------

You can use `rebar` for the compilation or just use the `Makefile`

.. sourcecode:: bash

   rebar compile

or

.. sourcecode:: bash

   make

Usage
-----

There is `document_api:start/0` function you can use it to launch the HTTP Api
server.

.. sourcecode:: bash

   erl -pa deps/*/ebin -pa apps/*/ebin -s document_api

Tests
-----

.. sourcecode:: bash

   > pip install pytest
   > py.test -v -x -k tests 
