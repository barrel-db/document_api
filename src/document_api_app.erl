-module(document_api_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).
% -export([is_valid_dbname/1]).
-export([debug_hook/1]).
-export([debug_hook/4]).
    
-define(C_ACCEPTORS, 100).
-define(CONSTRAINTS, [{database, function, fun document_api_util:is_valid_dbname/1}]).


%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    Routes = get_routes(),
    Dispatch = cowboy_router:compile(Routes),
    TransportOpts = [
        {port, 8080},
        {ip, {0, 0, 0, 0}}
    ],

    ProtocolOpts = [
        {compress, true},
        {onrequest, fun ?MODULE:debug_hook/1},
        {onresponse, fun ?MODULE:debug_hook/4},
        {env, [{dispatch, Dispatch}]
    }],
    {ok, _} = cowboy:start_http(http, ?C_ACCEPTORS,
                                TransportOpts,
                                ProtocolOpts),
    document_api_sup:start_link().

stop(_State) ->
    ok.

debug_hook(Status, Headers, Body, Req) ->
    % io:format("Status: ~p~n", [Status]),
    Req.

debug_hook(Req) ->
    {Method, _} = cowboy_req:method(Req),
    {PathInfo, _} = cowboy_req:path(Req),
    io:format("~s ~n", [<<Method/binary, " ", PathInfo/binary>>]),
    % erlang:display(Req),
    Req.


%% Private functions
get_routes() ->
    [
     {'_', [
        {"/:database",
            ?CONSTRAINTS,
            database_handler, []},

        {"/:database/_all_docs",
            ?CONSTRAINTS,
            database_all_docs_handler, []},

        {"/:database/_bulk_docs",
            ?CONSTRAINTS,
            database_bulk_docs_handler, []},

        {"/:database/_changes",
            ?CONSTRAINTS,
            database_changes_handler, []},

        {"/:database/_missing_revs",
            ?CONSTRAINTS,
            database_missing_revs_handler, []},

        {"/:database/:document",
            ?CONSTRAINTS,
            database_handler, []},

        {"/:database/:document/:attachment",
            ?CONSTRAINTS,
            database_handler, []}]}
    ].

