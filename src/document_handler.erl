-module(document_handler).

-export([
    init/3,
    allowed_methods/2,
    content_types_accepted/2,
    content_types_provided/2,
    get_json/2,
    put_json/2
]).

init(_Transport, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
    {[<<"HEAD">>, <<"GET">>, <<"PUT">>, <<"DELETE">>, <<"COPY">>], Req, State}.

content_types_provided(Req, State) ->
    {[{{<<"application">>, <<"json">>, []}, get_json}], Req, State}.

content_types_accepted(Req, State) ->
    {[{{<<"application">>, <<"json">>, []}, put_json}], Req, State}.

put_json(Req, State) ->
    %% Fetch the method and the database
    {Method, _} = cowboy_req:method(Req),
    {Database, _} = cowboy_req:binding(database, Req),
    {Document, _} = cowboy_req:binding(document, Req),

    %% Compute the result
    {ok, Response, Req2, State2} = process(Method, Database, Document, Req, State),

    %% Set the body
    Body = jsx:encode(Response),
    Req3 = cowboy_req:set_resp_body(Body, Req2),

    {true, Req3, State2}.

get_json(Req, State) ->
    {Method, _} = cowboy_req:method(Req),
    {Database, _} = cowboy_req:binding(database, Req),
    {Document, _} = cowboy_req:binding(document, Req),

    {Result, Req2, State2} = process(Method, Database, Document, Req, State),
    Body = jsx:encode(Result),

    {Body, Req2, State2}.

%% HEAD /{db}/{docid}
%% http://docs.couchdb.org/en/latest/api/document/common.html#head--db-docid
process(<<"HEAD">>, _Database, _Document, Req, State) ->
    {[], Req, State};

%% GET /{db}/{docid}
%% http://docs.couchdb.org/en/latest/api/document/common.html#get--db-docid
process(<<"GET">>, _Database, _Document, Req, State) ->
    Response = [
    ],
    {Response, Req, State};

%% PUT /{db}/{docid}
%% http://docs.couchdb.org/en/latest/api/document/common.html#put--db-docid
process(<<"PUT">>, _Database, _Document, Req, State) ->
    Response = [],
    {Response, Req, State};

%% DELETE /{db}/{docid}
%% http://docs.couchdb.org/en/latest/api/document/common.html#delete--db-docid
process(<<"DELETE">>, _Database, _Document, Req, State) ->
    {[{<<"ok">>, true}], Req, State};

%% COPY /{db}/{docid}
%% http://docs.couchdb.org/en/latest/api/document/common.html#copy--db-docid
process(<<"COPY">>, _Database, _Document, Req, State) ->
    Response = jsx:encode([
    ]),
    {ok, Response, Req, State}.
