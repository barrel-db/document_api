-module(database_all_docs_handler).

-export([
    init/3,
    allowed_methods/2,
    content_types_accepted/2,
    content_types_provided/2,
    get_json/2,
    put_json/2
]).

% -record(args, {
%     conficts = false :: boolean(),
%     descending = false :: boolean(),
%     endkey :: string(),
%     endkey_docid :: string(),
%     include_docs = false :: boolean(),
%     inclusive_end = false :: boolean(),
%     key :: string(),
%     limit :: integer(),
%     skip = 0 :: integer(),
%     stale :: string(),
%     startkey :: string(),
%     startkey_docid :: string(),
%     update_seq :: string()
% }).

init(_Transport, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
    {[<<"GET">>, <<"POST">>], Req, State}.

content_types_provided(Req, State) ->
    {[{{<<"application">>, <<"json">>, []}, get_json}], Req, State}.

content_types_accepted(Req, State) ->
    {[{{<<"application">>, <<"json">>, []}, put_json}], Req, State}.

put_json(Req, State) ->
    {Method, _} = cowboy_req:method(Req),
    {Database, _} = cowboy_req:binding(database, Req),

    {ok, Response, Req2, State2} = process(Method, Database, Req, State),

    Body = jsx:encode(Response),
    Req3 = cowboy_req:set_resp_body(Body, Req2),

    {true, Req3, State2}.

get_json(Req, State) ->
    {Method, _} = cowboy_req:method(Req),
    {Database, _} = cowboy_req:binding(database, Req),

    {Result, Req2, State2} = process(Method, Database, Req, State),
    Body = jsx:encode(Result),

    {Body, Req2, State2}.

%% GET /{db}/_all_docs
%% http://docs.couchdb.org/en/latest/api/database/bulk-api.html#get--db-_all_docs
process(<<"GET">>, _Database, Req, State) ->
    Props = [
        {conflicts, document_api_util:query_get_value(Req, conflicts, false)},
        {descending, document_api_util:query_get_value(Req, descending, false)},
        {endkey, document_api_util:query_get_value(Req, endkey)},
        {endkey_docid, document_api_util:query_get_value(Req, endkey_docid)},
        {include_docs, document_api_util:query_get_value(Req, include_docs, false)},
        {inclusive_end, document_api_util:query_get_value(Req, inclusive_end, false)},
        {key, document_api_util:query_get_value(Req, key)},
        {limit, document_api_util:query_get_value(Req, limit)},
        {skip, document_api_util:query_get_value(Req, skip, 0)},
        {stale, document_api_util:query_get_value(Req, stale)},
        {startkey, document_api_util:query_get_value(Req, startkey)},
        {startkey_docid, document_api_util:query_get_value(Req, startkey_docid)},
        {update_seq, document_api_util:query_get_value(Req, update_seq, false)}
    ],
    io:format("~p~n", [Props]),

    Response = [
        {<<"offset">>, 0},
        {<<"rows">>, []},
        {<<"total_rows">>, 0},
        {<<"update_seq">>, 0}
    ],

    {Response, Req, State};

%% POST /{db}/_all_docs
%% http://docs.couchdb.org/en/latest/api/database/bulk-api.html#post--db-_all_docs
process(<<"POST">>, _Database, Req, State) ->
    {ok, Content, _} = cowboy_req:body(Req),

    Body = jsx:decode(Content),

    io:format("Body: ~p~n", [Body]),

    Rows = [],

    Response = [
        {<<"total_rows">>, length(Rows)},
        {<<"offset">>, 0},
        {<<"rows">>, Rows}
    ],

    {ok, Response, Req, State}.
