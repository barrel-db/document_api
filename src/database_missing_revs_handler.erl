-module(database_missing_revs_handler).

-export([
    init/3,
    allowed_methods/2,
    content_types_accepted/2,
    put_json/2
]).

init(_Transport, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
    {[<<"GET">>, <<"POST">>], Req, State}.

content_types_accepted(Req, State) ->
    {[{{<<"application">>, <<"json">>, []}, put_json}], Req, State}.

put_json(Req, State) ->
    {Method, _} = cowboy_req:method(Req),
    {Database, _} = cowboy_req:binding(database, Req),

    {ok, Response, Req2, State2} = process(Method, Database, Req, State),

    Body = jsx:encode(Response),
    Req3 = cowboy_req:set_resp_body(Body, Req2),

    {true, Req3, State2}.

%% POST /{db}/_missing_revs
%% http://docs.couchdb.org/en/latest/api/database/misc.html#db-missing-revs
process(<<"POST">>, _Database, Req, State) ->
    {ok, Content, _} = cowboy_req:body(Req),
    Body = jsx:decode(Content),
    io:format("Body: ~p~n", [Body]),
    Map = maps:new(),
    Response = [
        {<<"missed_revs">>, Map}
    ],
    {ok, Response, Req, State}.