-module(database_changes_handler).

-export([
    init/3,
    allowed_methods/2,
    content_types_accepted/2,
    content_types_provided/2,
    get_json/2,
    put_json/2
]).

init(_Transport, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

allowed_methods(Req, State) ->
    {[<<"GET">>, <<"POST">>], Req, State}.

content_types_provided(Req, State) ->
    {[{{<<"application">>, <<"json">>, []}, get_json}], Req, State}.

content_types_accepted(Req, State) ->
    {[{{<<"application">>, <<"json">>, []}, put_json}], Req, State}.

put_json(Req, State) ->
    {Method, _} = cowboy_req:method(Req),
    {Database, _} = cowboy_req:binding(database, Req),

    {ok, Response, Req2, State2} = process(Method, Database, Req, State),

    Body = jsx:encode(Response),
    Req3 = cowboy_req:set_resp_body(Body, Req2),

    {true, Req3, State2}.

get_json(Req, State) ->
    {Method, _} = cowboy_req:method(Req),
    {Database, _} = cowboy_req:binding(database, Req),

    {Result, Req2, State2} = process(Method, Database, Req, State),
    Body = jsx:encode(Result),

    {Body, Req2, State2}.

%% GET /{db}/_changes
%% http://docs.couchdb.org/en/latest/api/database/changes.html#get--db-_changes
process(<<"GET">>, _Database, Req, State) ->
    Response = [
        {<<"last_seq">>, 0},
        {<<"results">>, [
            [{<<"changes">>, []},
             {<<"id">>, <<"1234567890">>},
             {<<"seq">>, 1}]
            ]
        }
    ],

    {Response, Req, State};

%% POST /{db}/_changes
%% http://docs.couchdb.org/en/latest/api/database/changes.html#post--db-_changes
process(<<"POST">>, _Database, Req, State) ->
    {ok, Content, _} = cowboy_req:body(Req),
    Body = jsx:decode(Content),
    io:format("Body: ~p~n", [Body]),
    Response = [
        {<<"last_seq">>, 30},
        {<<"results">>, []}
    ],
    {ok, Response, Req, State}.