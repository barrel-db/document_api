-module(document_api_util).

-export([
    query_get_value/2,
    query_get_value/3,
    binary_to_lower/1,
    parse_boolean/1,
    parse_int/1,
    is_valid_dbname/1,
    convert_body/2
]).

query_get_value(Req, Key) ->
    {Value, _} = cowboy_req:qs_val(
        erlang:atom_to_binary(Key, latin1),
        Req
    ),
    Value.

query_get_value(Req, Key, Default) ->
    {Value, _} = cowboy_req:qs_val(
        erlang:atom_to_binary(Key, latin1),
        Req, Default
    ),
    Value.

binary_to_lower(Val) when is_binary(Val) ->
    string:to_lower(erlang:binary_to_list(Val)).

parse_boolean(true) -> true;

parse_boolean(false) -> false;

parse_boolean(Val) when is_binary(Val) ->
    parse_boolean(binary_to_list(Val));

parse_boolean(Val) ->
    case string:to_lower(Val) of
        "true" -> true;
        "false" -> false;
        _ ->
            Msg = io_lib:format("Invalid boolean parameter: ~p", [Val]),
            throw({query_parse_error, list_to_binary(Msg)})
    end.

parse_int(Val) when is_integer(Val) -> Val;
parse_int(Val) when is_binary(Val) ->
    parse_int(binary_to_integer(Val));
parse_int(Val) ->
    case (catch list_to_integer(Val)) of
        IntVal when is_integer(IntVal) ->
            IntVal;
        _ ->
            Msg = io_lib:format("Invalid value for integer: ~p", [Val]),
            throw({query_parse_error, list_to_binary(Msg)})
    end.

is_valid_dbname(Dbname) ->
    Pattern = "^[a-z][a-z0-9_$()+/-]*$",
    case re:run(Dbname, Pattern) of
        {match, _} -> true;
        nomatch -> false
    end.

convert_body(Body, Req) ->
    case cowboy_req:meta(media_type, Req) of
        {MediaType, _} ->
            do_convert_body(Body, MediaType);
        undefined ->
            <<>>
    end.

do_convert_body(Body, {<<"application">>, <<"json">>, []}) ->
    jsx:encode(Body);

do_convert_body(Body, {<<"application">>, <<"x-msgpack">>, []}) ->
    msgpack:packb(Body, [{format, jsx}]);

do_convert_body(Body, _) ->
    Body.