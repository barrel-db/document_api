-module(document_api_fake).

-compile(export_all).
% -export([
%     get/1,
%     validate_docid/1,
%     put/2,
%     exists/1
% ]).

get(DocId) ->
    ok.

validate_docid("qwertyuiop") ->
    false;
validate_docid(DocId) ->
    true.

put(Db, Doc) ->
    ok.

exists(Doc) ->
    true.
