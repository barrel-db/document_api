-module(document_api).

-export([start/0]).

start() ->
    application:ensure_all_started(document_api).