-module(database_handler).

-export([
    init/3,
    rest_init/2,

    allowed_methods/2,
    content_types_accepted/2,
    content_types_provided/2,

    resource_exists/2,
    delete_resource/2,
    delete_completed/2,

    save_json/2,
    save_msgpack/2,

    return_json/2,
    return_msgpack/2
]).

-record(params, {
    committed_update_seq :: integer(),
    compact_running :: boolean(),
    db_name :: string(),
    disk_format_version :: integer(),
    data_size :: integer(),
    disk_size :: integer(),
    doc_count :: integer(),
    doc_del_count :: integer(),
    instance_start_time :: string(),
    purge_seq :: integer(),
    update_seq :: integer()
}).

-record(state, {
    database :: string(),
    response = undefined :: term()
}).

% -ifdef(EUNIT).
-define(BACKEND, document_api_fake).
% -else.
% -define(BACKEND, enki_doc).
% -endif.

init(_Transport, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

rest_init(Req, _) ->
    {Database, _} = cowboy_req:binding(database, Req),
    {ok, Req, #state{database=Database}}.

allowed_methods(Req, State) ->
    Methods = [<<"HEAD">>, <<"GET">>, <<"PUT">>, <<"DELETE">>, <<"POST">>],
    {Methods, Req, State}.

content_types_provided(Req, State) ->
    Json = {{<<"application">>, <<"json">>, []}, return_json},
    MsgPack = {{<<"application">>, <<"x-msgpack">>, []}, return_msgpack},

    ContentTypes = [Json, MsgPack],

    {ContentTypes, Req, State}.

content_types_accepted(Req, State) ->
    Json = {{<<"application">>, <<"json">>, []}, save_json},
    MsgPack = {{<<"application">>, <<"x-msgpack">>, []}, save_msgpack},

    ContentTypes = [Json, MsgPack],

    {ContentTypes, Req, State}.

save_msgpack(Req, #state{database=Database}=State) ->
    {Method, Req2} = cowboy_req:method(Req),
    FullPath = full_path(Database),

    case Method of
        <<"PUT">> ->
            ok = file:write_file(FullPath, <<>>),
            Req3 = cowboy_req:set_resp_header(
                <<"location">>, <<$/, Database/binary>>, Req2
            ),
            {true, Req3, State};

        <<"POST">> ->
            {ok, Body, _} = cowboy_req:body(Req2),

            {ok, Content} = msgpack:unpack(Body),
            Body2 = jsx:encode(Content),

            ok = file:write_file(FullPath, Body2, [append, write, sync]),

            Req3 = cowboy_req:set_resp_header(
                <<"location">>, <<$/, Database/binary>>, Req2
            ),
            {ok, Req4} = cowboy_req:reply(201, Req3),
            {halt = Req4, State}
    end.

return_msgpack(Req, #state{response=Response}=State) ->
    Body = msgpack:pack(Response, [{format, jsx}]),
    {Body, Req, State}.

save_json(Req, #state{database=Database}=State) ->
    {Method, Req2} = cowboy_req:method(Req),
    FullPath = full_path(Database),

    case Method of
        <<"PUT">> ->
            ok = file:write_file(FullPath, <<>>),
            Req3 = cowboy_req:set_resp_header(
                <<"location">>, <<$/, Database/binary>>, Req2
            ),
            {true, Req3, State};
        <<"POST">> ->
            {ok, Body, _} = cowboy_req:body(Req2),

            ok = file:write_file(FullPath, Body, [append, write, sync]),

            Req3 = cowboy_req:set_resp_header(
                <<"location">>, <<$/, Database/binary>>, Req2
            ),
            {ok, Req4} = cowboy_req:reply(201, Req3),
            {halt, Req4, State}
    end.

return_json(Req, #state{response=Response}=State) ->
    Body = jsx:encode(Response),
    {Body, Req, State}.


parse_params(Req) ->
    {Params, _} = cowboy_req:qs_vals(Req),
    lists:foldl(fun do_parse_params/2, #params{}, Params).

do_parse_params({Key, Value}, Args) ->
    Param = document_api_util:binary_to_lower(Key),
    case {Param, Value} of
        {"committed_update_seq", _} ->
            Args#params{committed_update_seq=document_api_util:parse_int(Value)};
        {"compact_running", _} ->
            Args#params{compact_running=document_api_util:parse_boolean(Value)};
        {"db_name", _} ->
            true = document_api_util:is_valid_dbname(binary_to_list(Value)),
            Args#params{db_name=binary_to_list(Value)};
        {"disk_format_version", _} ->
            Args#params{disk_format_version=binary_to_list(Value)};
        {"data_size", _} ->
            Args#params{data_size=document_api_util:parse_int(Value)};
        {"disk_size", _} ->
            Args#params{disk_size=document_api_util:parse_int(Value)};
        {"doc_count", _} ->
            Args#params{doc_count=document_api_util:parse_int(Value)};
        {"doc_del_count", _} ->
            Args#params{doc_del_count=document_api_util:parse_int(Value)};
        {"instance_start_time", _} ->
            Args#params{instance_start_time=binary_to_list(Value)};
        {"purge_seq", _} ->
            Args#params{purge_seq=document_api_util:parse_int(Value)};
        {"update_seq", _} ->
            Args#params{update_seq=document_api_util:parse_int(Value)};
        _Else ->
            Args
    end.

do_resource_exists(<<"HEAD">>, Req, #state{database=Database}=State) ->
    Exists = ?BACKEND:exists(Database),
    {Exists, Req, State};
    % FileExists = file_exists(Database),
    % {FileExists, Req, State};

do_resource_exists(<<"GET">>, Req, #state{database=Database}=State) ->
    io:format("GET: ~p~n", [State]),
    _Params = parse_params(Req),
    Response = [
        {<<"committed_update_seq">>, 0},
        {<<"compact_running">>, true},
        {<<"db_name">>, Database}
    ],
    {true, Req, State#state{response=Response}};

do_resource_exists(<<"DELETE">>, Req, #state{database=Database}=State) ->
    FileExists = file_exists(Database),
    {FileExists, Req, State};

do_resource_exists(<<"POST">>, Req, #state{database=Database}=State) ->
    case file_exists(Database) of
        true -> {true, Req, State};
        false ->
            Response = [{<<"ok">>, false}],
            Body = document_api_util:convert_body(Response, Req),
            Req2 = cowboy_req:set_resp_body(Body, Req),
            {ok, Req3} = cowboy_req:reply(404, Req2),
            {halt, Req3, State}
    end;

do_resource_exists(<<"PUT">>, Req, #state{database=Database}=State) ->
    case file_exists(Database) of
        true ->
            Response = [
                {<<"error">>, <<"file_exists">>},
                {<<"reason">>, <<"The database could not be created, the file already exists">>}
            ],
            Body = document_api_util:convert_body(Response, Req),
            Req2 = cowboy_req:set_resp_body(Body, Req),
            {ok, Req3} = cowboy_req:reply(412, [], Req2),
            {halt, Req3, State};
        false ->
            Response = [{<<"ok">>, true}],
            Body = document_api_util:convert_body(Response, Req),
            Req2 = cowboy_req:set_resp_body(Body, Req),
            {false, Req2, State}
    end.

resource_exists(Req, State) ->
    {Method, _} = cowboy_req:method(Req),
    do_resource_exists(Method, Req, State).

delete_resource(Req, #state{database=Database}=State) ->
    {delete_file(Database), Req, State}.

delete_completed(Req, State) ->
    Response = [{<<"ok">>, true}],
    Body = document_api_util:convert_body(Response, Req),
    Req2 = cowboy_req:set_resp_body(Body, Req),
    {true, Req2, State}.

full_path(Name) when is_binary(Name) ->
    full_path(binary_to_list(Name));

full_path(Name) when is_list(Name) ->
    filename:join([code:priv_dir(document_api), Name]).

delete_file(Name) ->
    case file:delete(full_path(Name)) of
        ok -> true;
        {error, _Reason} -> false
    end.

file_exists(Name) ->
    case file:read_file_info(full_path(Name)) of
        {ok, _Info} -> true;
        {error, _Reason} -> false
    end.
