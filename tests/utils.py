import functools
import json
import msgpack
import requests
import attrdict
import logging

def enable_logging():
    try:
        import http.client as http_client
    except ImportError:
        # Python 2
        import httplib as http_client
    http_client.HTTPConnection.debuglevel = 1
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

def request_json(method, url, data=None):
    headers = {k: 'application/json' for k in ['content-type', 'accept']}

    if data is not None:
        data = json.dumps(data)

    return requests.request(method, url, headers=headers, data=data)

def request_msgpack(method, url, data=None):
    headers = {k: 'application/x-msgpack' for k in ['content-type', 'accept']}

    if data is not None:
        data = json.dumps(data)

    return requests.request(method, url, headers=headers, data=data)


delete_json = functools.partial(request_json, 'delete')
get_json = functools.partial(request_json, 'get')
head_json = functools.partial(request_json, 'head')
post_json = functools.partial(request_json, 'post')
put_json = functools.partial(request_json, 'put')

delete_msgpack = functools.partial(request_msgpack, 'delete')
get_msgpack = functools.partial(request_msgpack, 'get')
head_msgpack = functools.partial(request_msgpack, 'head')
post_msgpack = functools.partial(request_msgpack, 'post')
put_msgpack = functools.partial(request_msgpack, 'put')
