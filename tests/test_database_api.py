#!/usr/bin/env python
from .utils import (
    delete_json,
    get_json,
    head_json,
    post_json,
    put_json)

from .utils import (
    delete_msgpack,
    get_msgpack,
    head_msgpack,
    post_msgpack,
    put_msgpack
)

URL = 'http://localhost:8080/database'

def test_database_head():
    response = head_json(URL)
    assert response.status_code == 200


# response = get_msgpack('http://localhost:8080/database')
# print "Response: %r" % (msgpack.unpackb(response.content),)

# def head_json(url, data=None):
#     if data is not None:
#         data = json.dumps(data)
#     return requests.head(url, headers={'Content-Type': 'application/json'}, data=data)

# print post_json('http://localhost:8080/database')

# response = put_json('http://localhost:8080/database')
# print response
# print response.content

# print get_json('http://localhost:8080/database')

# def test_database_head():
#     response = head_json('http://localhost:8080/database')
#     assert response.status_code == 200

#     response = head_json('http://localhost:8080/nodb')
#     assert response.status_code == 404

# def test_database_get():
#     response = get_json('http://localhost:8080/database')
#     assert response.status_code == 200
#     db = attrdict.AttrDict(response.json())
#     assert db.db_name == 'database'

# def test_database_post():
#     response = post_json('http://localhost:8080/database')
#     # assert response.status_code == 200
#     # print response
#     # print response.content
#     # db = attrdict.AttrDict(response.json())
#     # print db
#     # assert db.id == 'document_id'
#     # assert db.ok == True
#     # assert db.rev == '1-0A0B0C0D0E'

# def test_database_all_docs_post():
#     data = {
#         'keys': ['qwertyuiop', 'zxcvbnm'],
#     }
#     response = post_json('http://localhost:8080/database/_all_docs', data)
#     assert response.status_code == 200

# def test_database_bulk_docs_post():
#     data = {
#         'all_or_nothing': False,
#         'docs': [],
#         'new_edits': True
#     }

#     response = post_json('http://localhost:8080/database/_bulk_docs', data)
#     assert response.status_code == 200
#     # print response

# def test_database_missing_revs_post():
#     data = {
#     }

#     response = post_json('http://localhost:8080/database/_missing_revs', data)
#     assert response.status_code == 200
#     db = attrdict.AttrDict(response.json())

#     assert isinstance(db.missed_revs, attrdict.AttrDict)
#     assert len(db.missed_revs) == 0
